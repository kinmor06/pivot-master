module Pivot
  class Tracker
    class << self
      # Count number of items passed
      # Params:
      # +items+:: +Array+ of hashes
      def count(items)
        items.count
      end

      # Find item from `items` haystack based on assignee value
      # Params:
      # +items+:: +Array+ of hashes
      # +assignee+:: string for finding the needle in the haystack
      def item_for(items, assignee)
        items_for_assignee(items, assignee).last
      end

      # Check if assignee is on the list
      # Params:
      # +items+:: +Array+ of hashes
      # +assignee+:: string for finding the needle in the haystack
      def pivoted?(items, assignee)
        !item_for(items, assignee).nil?
      end

      # Return the total points of the items, or per assignee
      # Params:
      # +items+:: +Array+ of hashes
      # +options+:: +Hash+ for optional argument
      def total_points(items, options = {})
        items = items_for_assignee(items, options[:assignee]) unless options[:assignee].nil?
        items.sum { |item| item[:points] }
      end

      # Return the total points of the items, or per assignee
      # Params:
      # +items+:: +Array+ of hashes
      def unique_assignees(items)
        items.uniq! { |item| item[:assignee] }
      end

      private

      def items_for_assignee(items, assignee)
        items.select { |item| item[:assignee] == assignee }
      end
    end
  end
end
