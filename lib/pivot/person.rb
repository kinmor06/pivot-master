module Pivot
  class Person
    attr_accessor :email, :first_name, :last_name, :items

    def initialize(params)
      @email      = params[:email]
      @first_name = params[:first_name]
      @last_name  = params[:last_name]
      @items = []
    end

    # Add item to Pivot::Person instance
    # +item+:: +Pivot::Item+ instance
    #
    # Used `yield` instead of explicit block
    # for performance and since spec doesn't require
    # having a Proc object and it's only calling the block
    def add_item(item)
      yield if block_given?
      item.assignee = email
      @items << item
    end
  end
end
