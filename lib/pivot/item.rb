module Pivot
  class Item
    VALID_PROJECT_CODES = ['AZR', 'EREC']

    attr_accessor :assignee, :name, :points

    def initialize(params = {})
      @assignee = params[:assignee]
      @name     = params[:name]
      @points   = params[:points]
    end

    # Adds a different Item's #points value
    # +other_item+:: +Pivot::Item+ instance
    def +(other_item)
      points + other_item.points
    end

    # Return the project code based on the :name attribute
    def project_code
      name.split('-').first
    end

    # Check validity of project code based on preset codes
    def valid?
      VALID_PROJECT_CODES.include?(project_code)
    end
  end
end
